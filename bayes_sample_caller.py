from prepare_data import PrepareData
from genomic_unit_caller import GenomicUnitCaller

class BayesSampleCaller:
    def __init__(self,sample_filename,kwargs):
        self.sample_filename = sample_filename
        self.kwargs = kwargs
        
    def convert_output_for_next_level(self,df, genome_unit_cols, given_1, given_0):
        cols = genome_unit_cols + [given_1, given_0]
        df = df.loc[:, cols]
        df = df.drop_duplicates()
        return df

    def call_genomic_unit_caller(self,df, \
                                 pos_threshold, \
                                 given_1, \
                                 given_0, \
                                 output_col_1, \
                                 output_col_0, \
                                 genome_unit_cols):
        guc = GenomicUnitCaller(df, pos_threshold, genome_unit_cols)
        output_df = guc.calculate_data_given_genom_unit_cond( \
            given_1, \
            given_0, \
            output_col_1, \
            output_col_0 \
            )
        return output_df

    def calculate_sample_posterior(self):
        '''
        This method calculates the sample posterior
        probability
        '''

        # Step 1: Prepare Data, calculate basic values
        prepData = PrepareData(self.sample_filename, self.kwargs)
        data = prepData.prepare_data()
        '''
        To test code, uncomment the following line:
        data = data[(data['sampleId'] == 'IP528-PLA-001-A') & (data['chr'].isin(['chr1', 'chrX'])) & (
            data['amplicon'].isin(['A-1', 'A-2', 'A-6']))]
        '''

        # Step 2: calculate the conditionals for position level

        given_1 = 'base_1_posterior'
        given_0 = 'base_0_posterior'
        output_col_1 = 'position_1_posterior'
        output_col_0 = 'position_0_posterior'
        pos_threshold = self.kwargs['base_positivity_threshold']
        genome_unit_cols = ['sampleId', 'chr', 'amplicon', 'pos']

        posterior_df = self.call_genomic_unit_caller(data, pos_threshold, given_1, given_0, output_col_1, output_col_0,
                                                genome_unit_cols)

        # Step 3: calculate the conditionals for amplicon level

        given_1 = 'position_1_posterior'
        given_0 = 'position_0_posterior'
        output_col_1 = 'amplicon_1_posterior'
        output_col_0 = 'amplicon_0_posterior'
        pos_threshold = self.kwargs['position_positivity_threshold']
        genome_unit_cols = ['sampleId', 'chr', 'amplicon']

        data = self.convert_output_for_next_level(posterior_df, genome_unit_cols, given_1, given_0)

        amplicon_level_df = self.call_genomic_unit_caller(data, pos_threshold, given_1, given_0, output_col_1, output_col_0,
                                                     genome_unit_cols)

        # Step 4: calculate the conditionals for chromosome level

        given_1 = 'amplicon_1_posterior'
        given_0 = 'amplicon_0_posterior'
        output_col_1 = 'chrom_1_posterior'
        output_col_0 = 'chrom_0_posterior'
        pos_threshold = self.kwargs['amplicon_positivity_threshold']
        genome_unit_cols = ['sampleId', 'chr']
        data = self.convert_output_for_next_level(amplicon_level_df, genome_unit_cols, given_1, given_0)

        chromosome_level_df = self.call_genomic_unit_caller(data, pos_threshold, given_1, given_0, output_col_1,
                                                       output_col_0,
                                                       genome_unit_cols)

        # Step 5: calculate the conditionals for sample level

        given_1 = 'chrom_1_posterior'
        given_0 = 'chrom_0_posterior'
        output_col_1 = 'sample_1_posterior'
        output_col_0 = 'sample_0_posterior'
        pos_threshold = self.kwargs['chromosome_positivity_threshold']
        genome_unit_cols = ['sampleId']
        data = self.convert_output_for_next_level(chromosome_level_df, genome_unit_cols, given_1, given_0)

        sample_level_df = self.call_genomic_unit_caller(data, pos_threshold, given_1, given_0, output_col_1, output_col_0,
                                                   genome_unit_cols)

        sample_level_df = self.convert_output_for_next_level(sample_level_df, genome_unit_cols, output_col_1, output_col_0)

        return sample_level_df.to_dict('index')[0]