import pandas as pd
import numpy as np

class PrepareData:

    def __init__(self,sample_filename,kwargs):
        #self.sample = pd.read_csv(kwargs['sample_filename'])
        self.sample = pd.read_csv(sample_filename)
        self.tissue_priors = pd.read_csv(kwargs['tissue_priors'])
        self.cancer_prevalence = pd.read_csv(kwargs['cancer_prevalence'],index_col=0)
        self.data_priors = pd.read_csv(kwargs['data_priors'],index_col=0)

    def create_binary_data(self):
        '''
        This method creates the binary data column referenced in
        the write up. eq.2
        '''
        self.sample['binary_data'] = np.where(self.sample.confidence > .5, 1, 0)

    def join_tissue_priors(self):
        '''
        This method joins the tissue_prior data to
        the sample data. These values correspond to
        list item 1 in the subsection 3.5.1 in the write-up.
        '''
        self.sample = self.sample.merge(
            self.tissue_priors.loc[:, [ \
                                          'chr', 'start', 'mut', 'prevalence'] \
            ], \
            how='inner', \
            left_on=['chr', 'pos', 'mut'],
            right_on=['chr', 'start', 'mut'])
        self.sample = self.sample.rename( \
            columns={'prevalence': 'tissue_prior_1'})

    def calculate_base_prior(self):
        '''
        This method calculates the base prior in eq. 34
        in the write-up, by taking the product of the
        cancer prevalence (item 2 in section 3.5.1) and
        the conditional probability in tissue_prior.
        '''
        self.sample['cancer_prevalence'] = \
            self.cancer_prevalence.at[0, 'cancer_prevalence']
        #self.sample['base_prior_1'] = np.product( \
        #    self.sample['tissue_prior_1'], \
        #    self.sample['cancer_prevalence']
        #)
        self.sample['base_1_prior'] = self.sample['tissue_prior_1']*\
                                      self.sample['cancer_prevalence']

    def join_prob_data(self):
        '''
        This method joins the prior
        probabilities of the data referenced
        in section 3.5.2
        '''
        self.sample = self.sample.merge(\
            self.data_priors, how='inner', on=['chr', 'pos', 'mut'])
        self.sample['data_obs_prior'] = np.where(\
            self.sample['binary_data']==1,\
            self.sample['data_1_prior'],\
            1 - self.sample['data_1_prior'])

    def calculate_prob_base_given_data(self):
        '''
        This method calculates P(base=1|data), described
        in section 3.5.3 in the write-up.
        '''
        self.sample['p_base_1_given_data'] = \
            1 - self.sample['null_likelihood']

    def select_relevant_columns(self):
        #self.sample['targetId_minus_mut'] = self.sample['targetId'].str[:-4]
        self.sample['amplicon'] = self.sample['targetId'].str[18:21]
        cols = ['sampleId',
                         #'targetId_minus_mut',
                         'chr',
                         'amplicon',
                         'pos',
                         'ref',
                         'mut',
                         'type',
                         'confidence',
                         'tumor_likelihood',
                         'null_likelihood',
                         'ctFraction',
                         'mutationPresent',
                         'errorCounts',
                         'refCounts',
                         'DOR',
                        'binary_data',
                        'start',
                        'tissue_prior_1',
                        'cancer_prevalence',
                        'base_1_prior',
                        'data_1_prior',
                        'data_obs_prior',
                        'p_base_1_given_data'
                ]
        self.sample = self.sample.loc[:,cols]

    def calculate_base_posteriors(self):
        '''
        This method calculates the base
        posteriors, eq. 33.
        '''
        self.sample['base_1_posterior'] = self.sample['p_base_1_given_data']*\
                                      self.sample['data_obs_prior'] /\
                                      self.sample['base_1_prior']
        self.sample['base_0_posterior'] = (self.sample['p_base_1_given_data'])*\
                                      self.sample['data_obs_prior'] /\
                                      (1 - self.sample['base_1_prior'])
        print('***Calculated base posteriors***')

    def prepare_data(self):
        '''
        Prepare data for posterior prob calculations
        '''
        self.create_binary_data()
        self.join_tissue_priors()
        self.calculate_base_prior()
        self.join_prob_data()
        self.calculate_prob_base_given_data()
        self.sample = self.sample.sort_values(['chr','pos','ref','mut'])
        self.select_relevant_columns()
        self.calculate_base_posteriors()
        return self.sample
