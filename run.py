import pandas as pd
from bayes_sample_caller import BayesSampleCaller

def run(kwargs):
	samples_list = []
	for sample_filename in kwargs['sample_filenames']:
		bsc = BayesSampleCaller(sample_filename,kwargs)
		output_dict = bsc.calculate_sample_posterior()
		samples_list.append(output_dict)
	output_df = pd.DataFrame(samples_list)
	return output_df

if __name__=="__main__":

	kwargs = {
		'sample_filenames':['targets.csv','targets.csv'],
		'tissue_priors':'prevalence.csv',
		'cancer_prevalence': 'cancer_prevalence.csv',
		'data_priors': 'data_priors.csv',
		'base_positivity_threshold':1,
		'position_positivity_threshold': 1,
		'amplicon_positivity_threshold': 1,
		'chromosome_positivity_threshold': 1
	}

	samples_df = run(kwargs)
	samples_df.to_csv('results.csv')
	print(f"samples_df = \n{samples_df}")
