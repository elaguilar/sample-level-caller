
class GenomicUnitCaller:
    def __init__(self,df,pos_threshold,genome_unit_cols):
        self.df = df
        self.pos_threshold = pos_threshold
        self.genome_unit_cols = genome_unit_cols

    def generate_all_perm_sum_geq_to_pos_threshold(self, size):
        '''
        This method generates all permutations of
        binary elements whose sum is greater than
        or equal to self.pos_threshold
        '''
        list_of_lists = []
        for r in range(self.pos_threshold - 1, size):
            bin_list = [1 if i < r + 1 else 0 for i in range(size)]
            for s in range(size):
                rot_list = [bin_list[(i + s) % len(bin_list)] \
                            for i, x in enumerate(bin_list)]
                if rot_list not in list_of_lists:
                    list_of_lists.append(rot_list)
        return list_of_lists

    def generate_all_perm_sum_lt_to_pos_threshold(self, size):
        '''
        This method generates all permutations of
        binary elements whose sum is less than self.pos_threshold
        '''
        list_of_lists = []
        for r in range(self.pos_threshold):
            bin_list = [1 if i < r else 0 for i in range(size)]
            for s in range(size):
                rot_list = [bin_list[(i + s) % len(bin_list)] \
                            for i, x in enumerate(bin_list)]
                if rot_list not in list_of_lists:
                    list_of_lists.append(rot_list)
        return list_of_lists

    def find_length_permutation(self,df):
        size = None
        df = df.groupby(self.genome_unit_cols).agg(['count'])
        df.columns = ['_'.join(x) for x in df.columns]
        size = df.iloc[0][self.given_1+'_count']
        return size

    def prob_data_given_genomic_unit_is_1(self,df,\
                                          given_1,\
                                          given_0,\
                                          output_col):
        if df.empty == False:
            cols = self.genome_unit_cols + [given_1, given_0]
            df = df.loc[:,cols]
            b_dict = {0: given_0, 1: given_1}
            sum_prods = 0
            size = df.shape[0]
            list_of_lists = self.generate_all_perm_sum_geq_to_pos_threshold(size)
            for permut in list_of_lists:
                running_prod = 1
                for ind in range(df.shape[0]):
                    row = df.iloc[ind]
                    l = permut[ind]
                    running_prod = running_prod * row[b_dict[l]]
                sum_prods += running_prod
            df[output_col] = sum_prods
            return df

    def prob_data_given_genomic_unit_is_0(self,df,\
                                          given_1,\
                                          given_0,\
                                          output_col):
        if df.empty == False:
            cols = self.genome_unit_cols + [given_1, given_0]
            df = df.loc[:,cols]
            b_dict = {0: given_0, 1: given_1}
            sum_prods = 0
            size = df.shape[0]
            list_of_lists = self.generate_all_perm_sum_lt_to_pos_threshold(size)
            for permut in list_of_lists:
                running_prod = 1
                for ind in range(df.shape[0]):
                    row = df.iloc[ind]
                    l = permut[ind]
                    running_prod = running_prod * row[b_dict[l]]
                sum_prods += running_prod
            df[output_col] = sum_prods
            return df

    def calculate_data_given_genom_unit_cond(self,\
                                            given_1,\
                                            given_0,\
                                            output_col_1,\
                                            output_col_0):
        self.given_1 = given_1
        z = self.df.groupby(self.genome_unit_cols).apply(\
            lambda rows: self.prob_data_given_genomic_unit_is_1(rows,\
                                                                given_1,\
                                                                given_0,\
                                                                output_col_1))
        y = self.df.groupby(self.genome_unit_cols).apply(\
            lambda rows: self.prob_data_given_genomic_unit_is_0(rows,\
                                                                given_1,\
                                                                given_0,\
                                                                output_col_0))
        z = z.reset_index()
        y = y.reset_index()
        genomic_unit_level_df = z.merge(y, how='inner',\
                                    on = self.genome_unit_cols+\
                                        [given_1,\
                                        given_0])
        return genomic_unit_level_df